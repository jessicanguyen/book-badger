from django.contrib import admin
from .models import Book

# admin.site.register(Book)

class BookAdmin(admin.ModelAdmin):
    """
    Book admin class
    """
    # search_fields = ('name', 'id')
    # list_filter = ('creation_date','modification_date') 
    list_display = ('id', 'title', 'author', 'genre', 'rating','amazon_price')
    

admin.site.register(Book, BookAdmin)

