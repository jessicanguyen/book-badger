from django.shortcuts import render
from .models import Book #since views.py and models.py in same directory, there is "."
import re 
from django.db.models import Q
from django.db.models import Count
import datetime
from operator import attrgetter

allbooks = Book.objects.all()

#Sends view to list mode
def list_view(request):
    template_name = 'listview/book_list.html'
    return book_list(request, template_name)

#Sends view to modal in list mode
def list_modal(request):
    template_name = 'listview/modal.html'
    return book_list(request, template_name)

#Sends view to cover mode
def cover_mode(request):
    template_name = 'covermode/main.html'
    return book_list(request, template_name)

#Given a date, if not a Monday date, edits it to fall on the previous Monday
#Date given and returned in format YYYY-MM-DD
def edit_date(date):
    date_list = date.split("-")
    year = date_list[0]
    month = date_list[1]
    day = date_list[2]
    temp = datetime.date(int(year),int(month),int(day))
    while temp.weekday() != 6:
        temp = temp - datetime.timedelta(days=1)
    return str(temp)

#Calculates books to show, given genre and start end dates (optional)
def book_list(request, template_name):
    booksPerPage = 50
    debug = 0

    allbooks = Book.objects.all()

    if 'genre' in request.GET:
        genre = request.GET['genre']
        if genre is None or genre == "":
            genre = "All"
    else:
        genre = "All"

    if 's_date' in request.GET:
        s_date = request.GET['s_date']
    else:
        s_date = "18-9-15"

    if 'e_date' in request.GET:
        e_date = request.GET['e_date']
    else:
        e_date = "24-9-15"
    
    if 'page' in request.GET:
        page = request.GET['page']
    else:
        page = "1"

    if 'sort' in request.GET:
        sort = request.GET['sort']
    else:
        sort = "rank"

    print("s_date: ")
    print(s_date)
    print("e_date: ")
    print(e_date)
    # if s_date == "":
    #     s_date = "27-9-15" #default values
    #     e_date = "3-10-15"
    allbooks = Book.objects.all()
    s_list = s_date.split("-")
    print("s_list: ")
    print(s_list)
    if len(s_list[0]) == 1:
        s_list[0] = "0"+s_list[0]
    if len(s_list[1]) == 1:
        s_list[1] = "0"+s_list[1]
    s_list[2] = "20"+s_list[2]
    start_date = s_list[2]+"-"+s_list[1]+"-"+s_list[0]
    print("start_date: " + start_date)
    
    e_list = e_date.split("-")
    if len(e_list[0]) == 1:
        e_list[0] = "0"+e_list[0]
    if len(e_list[1]) == 1:
        e_list[1] = "0"+e_list[1]
    e_list[2] = "20"+e_list[2]
    end_date = e_list[2]+"-"+e_list[1]+"-"+e_list[0]
    print("end_date: " + end_date)
    
    print("Changing start/end dates to be the nearest Monday in the past...")
    start_date = edit_date(start_date)
    end_date = edit_date(end_date)
    print("start_date: " + start_date)
    print("end_date: "+ end_date)

    q = Q(weeks_list__contains=start_date)
    delta = datetime.timedelta(days=7)
    next_date = start_date
    loop = 1
    while loop == 1:
        next_date_list = next_date.split("-")
        next_date_object = datetime.date(int(next_date_list[0]), int(next_date_list[1]), int(next_date_list[2]))
        next_date_object = next_date_object + delta
        next_date = str(next_date_object)
        print("next_date: ")
        print(next_date)
        q.add((Q(weeks_list__contains=next_date)), q.OR)
        if next_date == end_date:
            loop = 0

    if genre != "All":
        print("Genre filter applied.")
        q.add((Q(genre=genre)), q.AND)

    print(q)
    allbooks = allbooks.filter(q)

    for book in allbooks:
        if debug == 1:
            print("for book: "+book.title)
        total = 0
        index = 0

        temp = book.weeks_list #Returns as string with format [A,B,C]
        if debug == 1:
            print("book.weeks_list: "+ temp)
        temp = re.sub(r"\[", "", temp)
        temp = re.sub(r"\]", "", temp)
        week_list = temp.split(",")
        new_week_list = []
        for week in week_list:
            week = re.sub(r"\'", "", week)
            week = re.sub(r" ", "", week)
            week = str(week)
            week_array = week.split("-")
            week_obj = datetime.date(int(week_array[0]), int(week_array[1]), int(week_array[2]))
        
            new_week_list.append(week_obj)

        temp = book.weekly_rank #Returns as string with format [A,B,C]
        if debug == 1:
            print("book.weekly_rank: "+ temp)
        temp = re.sub(r"\[", "", temp)
        temp = re.sub(r"\]", "", temp)
        rank_list = temp.split(",")        
        new_rank_list = []    
        for rank in rank_list:
            rank = re.sub(r"\'", "", rank)
            rank = re.sub(r" ", "", rank)
            rank = int(rank)
            new_rank_list.append(rank)

            sd_list = start_date.split("-")
            sd_obj = datetime.date(int(sd_list[0]), int(sd_list[1]), int(sd_list[2]))
            ed_list = end_date.split("-")
            ed_obj = datetime.date(int(ed_list[0]), int(ed_list[1]), int(ed_list[2]))


        for week in new_week_list:

            if new_week_list[index] >= sd_obj:
                if new_week_list[index] <= ed_obj:
                    total = total + 21 - int(new_rank_list[index])
                    index = index + 1
            #         print("total = "+ str(total))
            # print("index: "+str(index))
       
        book.rank = total
        book.save()
        if debug == 1:
            print("cumulative rank = "+ str(total))

    allbooks = allbooks.order_by('rank')

    rankCount = 1
    for book in allbooks:
        book.rank = int(rankCount)
        book.save()
        rankCount = rankCount+1
        # print("book: "+ book.title)
        # print("rank: "+ str(book.rank))
    
    # if sort == "rank":
        # allbooks = sorted(allbooks, key=attrgetter('rank'), reverse=True)
    if sort == "rating":
        allbooks = allbooks.order_by('-rating')
    if sort == "author":
        allbooks = allbooks.order_by('author')
    if sort == "title":
        allbooks = allbooks.order_by('title')
        
    
    bookCount = allbooks.count()
    if int(page) > 1:
        previous = int(page) - 1
    else:
        previous = 0
    print("bookCount: "+str(bookCount))
    
    print("page: "+str(page))
    print("bpp: "+str(booksPerPage))
    if (bookCount-booksPerPage*(int(page)-1) > 50):
        print("????")
        next = int(page) + 1
    else:
        next = 0

    page = int(page) - 1
    allbooks = allbooks[page*booksPerPage:page*booksPerPage+booksPerPage]
  
    #populate genres list for use in searchtools' dropdown
    genres = []
    for book in allbooks:
        genres.append(book.genre)
    genres = set(genres)
  
    return render(request, template_name, {'allbooks': allbooks, 'genres': genres, 'genre':genre, 's_date': s_date, 'e_date': e_date, 'sort':sort, 'page':page, 'next':next, 'previous':previous})

    #2015-09-15, 2015-09-08