from listview.models import Book
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    args = 'no args'
    help = 'Quick last-minute script for updating'

    def _update_books(self):

        # b = Book.objects.get(title="UNEXPECTED FATE")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "18809943"
        # b.genre = "Romance"
        # b.GR_image_url = "https://d.gr-assets.com/books/1410620898l/18809943.jpg"
        # b.rating = "4.27"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/18809943"
        # b.save()


        # b = Book.objects.get(title="THE MASTER")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "22471007"
        # b.genre = "Romance"
        # b.GR_image_url = "https://www.goodreads.com/book/show/22471007-the-master"
        # b.rating = "4.40"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/22471007"
        # b.save()


        # b = Book.objects.get(title="THE ACCIDENTAL EMPRESS")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "22609307"
        # b.genre = "Historical Fiction"
        # b.GR_image_url = "https://d.gr-assets.com/books/1424053674l/22609307.jpg"
        # b.rating = "3.81"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/22609307"    
        # b.save()


        # b = Book.objects.get(title="BEACH TOWN")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "23245594"
        # b.genre = "Womens Fiction"
        # b.GR_image_url = "https://www.goodreads.com/book/show/23245594-beach-town"
        # b.rating = "3.54"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/23245594"        
        # b.save()


        # b = Book.objects.get(title="LOVE, LAUGHTER, AND STEAMY EVER AFTERS")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "25416758"
        # b.genre = "Anthologies"
        # b.GR_image_url = "https://d.gr-assets.com/books/1430829381l/25416758.jpg"
        # b.rating = "4.24"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/25416758"        
        # b.save()


        # b = Book.objects.get(title="ROBERT B. PARKER'S KICKBACK")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "23281911"
        # b.genre = "Mystery"
        # b.GR_image_url = "https://d.gr-assets.com/books/1417982172l/23281911.jpg"
        # b.rating = "3.81"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/23281911"        
        # b.save()


        # b = Book.objects.get(title="WISH YOU WELL")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "87745"
        # b.genre = "Historical Fiction"
        # b.GR_image_url = "https://d.gr-assets.com/books/1441500949l/87745.jpg"
        # b.rating = "3.92"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/87745"        
        # b.save()

        # b = Book.objects.get(title="EIGHTH GRAVE AFTER DARK")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "22922356"
        # b.genre = "Fantasy"
        # b.GR_image_url = "https://d.gr-assets.com/books/1413161725l/22922356.jpg"
        # b.rating = "4.33"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/22922356"        
        # b.save()

        # b = Book.objects.get(title="THE SUMMER'S END")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "23492748"
        # b.genre = "Womens Fiction"
        # b.GR_image_url = "https://d.gr-assets.com/books/1427161171l/23492748.jpg"
        # b.rating = "4.21"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/23492748"        
        # b.save()

        # b = Book.objects.get(title="12-ALARM COWBOYS")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "25523398"
        # b.genre = "Romance"
        # b.GR_image_url = "https://d.gr-assets.com/books/1436852092l/25523398.jpg"
        # b.rating = "4.48"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/25523398"        
        # b.save()

        # b = Book.objects.get(title="THE MARRIAGE OF OPPOSITES")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "23492741"
        # b.genre = "Historical Fiction"
        # b.GR_image_url = "https://d.gr-assets.com/books/1438581390l/23492741.jpg"
        # b.rating = "3.86"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/23492741"       
        # b.save()

        # b = Book.objects.get(title="PURITY")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "23754479"
        # b.genre = "Literary Fiction"
        # b.GR_image_url = "https://d.gr-assets.com/books/1438958976l/23754479.jpg"
        # b.rating = "3.73"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/23754479"        
        # b.save()

        # b = Book.objects.get(title="UNDERCOVER")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "23436172"
        # b.genre = "Romance"
        # b.GR_image_url = "https://d.gr-assets.com/books/1439068769l/23436172.jpg"
        # b.rating = "3.97"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/23436172"        
        # b.save()

        # b = Book.objects.get(title="THE SOLOMON CURSE")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "24611672"
        # b.genre = "Adventure"
        # b.GR_image_url = "https://d.gr-assets.com/books/1431032642l/24611672.jpg"
        # b.rating = "4.10"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/24611672"        
        # b.save()

        # b = Book.objects.get(title="STAR WARS: AFTERMATH")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "25131600"
        # b.genre = "Science Fiction"
        # b.GR_image_url = "https://d.gr-assets.com/books/1426620007l/25131600.jpg"
        # b.rating = "3.17"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/25131600"        
        # b.save()

        # b = Book.objects.get(title="DARK GHOST")
        # print("Found book: "+ b.title)
        # b.goodreads_ID = "22152434"
        # b.genre = "Fantasy"
        # b.GR_image_url = "https://d.gr-assets.com/books/1425924817l/22152434.jpg"
        # b.rating = "4.04"
        # b.goodreads_URL = "http://www.goodreads.com/book/show/22152434"        
        # b.save()

#ABOVE ARE ALL DONE~~~~~~~~~~~

        b = Book.objects.get(title="ONLY A KISS")
        print("Found book: "+ b.title)
        b.goodreads_ID = "24453092"
        b.genre = "Romance"
        b.GR_image_url = "https://d.gr-assets.com/books/1425960503l/24453092.jpg"
        b.rating = "4.15"
        b.goodreads_URL = "http://www.goodreads.com/book/show/24453092"        
        b.save()

        b = Book.objects.get(title="HOLD ON")
        print("Found book: "+ b.title)
        b.goodreads_ID = "25664796"
        b.genre = "Romance"
        b.GR_image_url = "https://d.gr-assets.com/books/1433669025l/25664796.jpg"
        b.rating = "4.44"
        b.goodreads_URL = "http://www.goodreads.com/book/show/25664796"
        b.save()

        b = Book.objects.get(title="ARCHANGEL'S ENIGMA")
        print("Found book: "+ b.title)
        b.goodreads_ID = "17409842"
        b.genre = "Romance"
        b.GR_image_url = "https://d.gr-assets.com/books/1427145741l/17409842.jpg"
        b.rating = "4.47"
        b.goodreads_URL = "http://www.goodreads.com/book/show/17409842"        
        b.save()


        b = Book.objects.get(title="PROTECTING MELODY")
        print("Found book: "+ b.title)
        b.goodreads_ID = "25699338"
        b.genre = "War"
        b.GR_image_url = "https://d.gr-assets.com/books/1442598106l/25699338.jpg"
        b.rating = "4.38"
        b.goodreads_URL = "http://www.goodreads.com/book/show/25699338"
        b.save()


        b = Book.objects.get(title="NUMBERS")
        print("Found book: "+ b.title)
        b.goodreads_ID = "26026345"
        b.genre = "Romance"
        b.GR_image_url = "https://d.gr-assets.com/books/1438526121l/26026345.jpg"
        b.rating = "4.28"
        b.goodreads_URL = "http://www.goodreads.com/book/show/26026345"
        
        b.save()



    def handle(self, *args, **options):
        self._update_books()
