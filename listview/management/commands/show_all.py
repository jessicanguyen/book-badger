from django.core.management.base import BaseCommand, CommandError
from listview.models import Book

class Command(BaseCommand):
    help = 'Shows all book models'

    def _show_books(self):
       books = Book.objects.all()
       for book in books:
            print(book.getTitle())
            print(book.getAuthor())
            print("Isbns: "+book.getISBNs())
            print("weekly_rank: "+book.getWeekly_Rank())
            print("Weeks_list: "+book.getWeeks_List())
            print("Goodreads ID: "+book.getGoodreads_ID())
            print("NYT URL: "+book.getNYT_Image_URL())
            print("GR URL: "+book.getGR_Image_URL())
            print("Genre: "+book.getGenre())
            print("Goodreads rating: "+book.getRating())

    def handle(self, *args, **options):
        self._show_books()