#Given a NYTimes Bestseller list (1 week's worth), creates model for all the books on the list

#Notes from lecturer
    #It's fine to store all the data locally on a server
    #It's fine to have a script that you can run every week to update info 
        #as long as it doesn't require any extra user input i.e. editing the script

#Notes:
    #failure March-01
    #https://www.goodreads.com/search/index.xml?key=Pq4UJeA708Z85U4IyHuAQ&q=One%20Night:%20Unveiled%20Jodi%20Ellen%20Malpas
    #Chooses "correct" title but this title has no Goodreads information on it. Need workaround
    #One Night UnVeiled - 
    #Twelve Days
    #Blood Infernal
#These three needed to be hand-edited

from urllib.parse import urlencode
import urllib.request
from urllib.request import FancyURLopener
import re
import json
import unicodedata
import datetime
import time

from django.core.management.base import BaseCommand
from Levenshtein import distance
from bs4 import BeautifulSoup
import requests

from listview.models import Book


def _strip_accents(s):  # required for titles with accents i.e. dash above e in fiance
    return ''.join(c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn')

# ============================= Amazon pricing scrapeing functions ====================================================

# Prices on the right are not always taken from avaliable pricing options, eg:
# http://www.amazon.com/All-Light-We-Cannot-See-ebook/dp/B00DPM7TIG?tag=thenewyorktim-20

# Defult search for scraping from amazon
def search(soup, term):
    # Handles general cases where Paperback is given as the 3rd result, eg:
    # http://www.amazon.com/Make-Me-Jack-Reacher-Novel/dp/0804178771
    price = '0.00'
    print("using Search 1 on " + str(term))
    wrappers = soup.select('li.swatchElement span.a-list-item span.a-button span.a-button-inner')
    name = []
    name_list = []
    # First check that physical prices are avaliable
    for wrapper in wrappers:
        name = wrapper.select('a.a-button-text span')
        name_list.append(name[0].text)

    name_list.append('Mass Market Paperback')
    print("Searching from " + str(name_list))

    if (term not in name_list) and ('Hardcover' not in name_list):
        print("No phyisical prices found")
        return '0.00'

    term2 = str(term),"Mass Market Paperback"
    # Checks complete..
    for wrapper in wrappers:
        name = wrapper.select('a.a-button-text span')
        if name[0].text.strip() in term2:
            try:
                return wrapper.select('span.a-color-secondary')[0].text.strip()
            except IndexError:
                price = search2(soup, term2)
                return price

    if price == '0.00':
        print("No result found")
        price = search4(soup,term)
        return price

    if price == '0.00':
        return price

# Search case 1: Only 2 prices are given, wraper used here is also different
def search2(soup, term):
    # Handles case where only 2 prices are given, eg:
        # http://www.amazon.com/Hollywood-Dirt-Alessandra-Torre/dp/1940941717
    print("using Search 2 on " + str(term))
    wrappers = soup.select('li.swatchElement span.a-list-item span.a-button span.a-button-inner')
    for wrapper in wrappers:
        name = wrapper.select('a.a-button-text span')
        if name[0].text.strip() in term:
            try:
                return wrapper.select('span.a-color-price')[0].text.strip()
            except TypeError:
                return wrapper.select('span')[0].text.strip()

# Search case 2: Only 1 price is avaliable
def search4(soup, term):
    # Handels case where only 2 preices are given, eg:
    # http://www.amazon.com/Aftermath-Star-Journey-Force-Awakens/dp/034551162X
    print("using Search 4 on " + str(term))
    wrappers = soup.select('li.swatchElement span.a-list-item span.a-button span.a-button-inner')
    for wrapper in wrappers:
        name = wrapper.select('a.a-button-text span')
        print(name[0].text)
        name2 = str(term), 'Hardcover'
        if name[0].text.strip() in name2:
            print("in if")
            return wrapper.select('span.a-color-price')[0].text.strip()

    if price == '0.00':
        return '0.00'

def cleanPrice(str):
    price = re.sub("From", '', str)
    price = re.sub("from", '', price)
    price = re.sub("\$", '', price)
    clean_price = re.sub(" ", '', price)
    return clean_price

class MyOpener(FancyURLopener):
    version = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11'

# ====================================================================================================================


class Command(BaseCommand):
    args = 'no args'
    help = 'Given a NYTimes API link, this creates book objects for all new books in the link'

    def _create_books(self):
        dates = ["2015-08-23"]

        # file = open('testbook.txt', 'r')        
        # input = file.read()

        # date = "2015-09-15"
        # file = open('testbook2.txt', 'r')        
        # input = file.read()

        #['2015-09-06', '2015-09-13', '2015-09-20', '2015-09-27'] 
        #'2015-01-04', '2015-01-11', '2015-01-18', '2015-01-25', '2015-02-01', '2015-02-08', '2015-02-15', '2015-02-22', '2015-03-01', '2015-03-08', '2015-03-15', '2015-03-22', '2015-03-29', '2015-04-05','2015-04-12', '2015-04-19', '2015-04-26', '2015-05-03', '2015-05-10', '2015-05-17', '2015-05-24', '2015-05-31', '2015-06-07','2015-06-14', '2015-06-21', '2015-06-28', '2015-07-05', '2015-07-12', '2015-07-19', '2015-07-26','2015-08-02', '2015-08-09', '2015-08-16', '2015-08-23', '2015-08-30', '2015-09-06', '2015-09-13', '2015-09-20', 
        #dates = ['2015-09-06', '2015-09-13', '2015-09-20', '2015-09-27']        
        #, '2015-08-02', '2015-08-09', '2015-08-16', '2015-08-23', '2015-08-30', '2015-09-06', '2015-09-13', '2015-09-20', '2015-09-27']
        debug = 0
        for date in dates:
            print("Running search for :"+date)

            url = "http://api.nytimes.com/svc/books/v3/lists/" + date + \
                  "/combined-print-and-e-book-fiction?api-key=59a1ab5ad1cb73060391028ba36dfc86:8:72695504"
            response = urllib.request.urlopen(url)
            html = response.read()
            input = html.decode()

            parsed_input=json.loads(input)
            results = parsed_input['results']
            books = results['books'] 

            temporary_books = []    #List containing dictionaries with structure {title, author, isbn, goodreads_ID}                
                                        #With one dictionary for each ISBN
            for book in books:
                contributor = book['contributor'] #can't use book['author'] as there is mistake in API 
                contributor = re.sub(r"by ", '', contributor)

                titleExists = Book.objects.filter(title=book['title'])
                authorExists = Book.objects.filter(author=contributor)
                if titleExists and authorExists:
                    getBook = Book.objects.get(title=book['title'], author=contributor)
                    if debug == 1:
                        print("Book already exists. Updating rank and week lists for :" + book['title'])
                    rank_list = getBook.weekly_rank  # Returns as string with format [A,B,C]
                    current_amazon_link = book['amazon_product_url']
                    # print("the url is " + str(current_amazon_link))
                    rank = book['rank']
                    rank = str(rank)
                    rank_list = re.sub(r"\[", "", rank_list)
                    rank_list = re.sub(r"\]", "", rank_list)
                    original_rank_list = rank_list.split(",")
                    new_rank_list = []
                    for rank in original_rank_list:
                        rank = re.sub(r"\'", "", rank)
                        rank = re.sub(r"\"", "", rank)
                        rank = re.sub(r"\\", "", rank)
                        rank = re.sub(r" ", "", rank)
                        new_rank_list.append(rank)
                    rank = book['rank']
                    rank = str(rank)
                    new_rank_list.append(rank)
                    if debug == 1:
                        print("Updated new_rank_list: ")
                        print(new_rank_list)
                    getBook.weekly_rank = new_rank_list

                    weeks_list = getBook.weeks_list #Returns as string with format ['A','B','C']
                    weeks_list = re.sub(r"\[", "", weeks_list)
                    weeks_list = re.sub(r"\]", "", weeks_list)
                    original_weeks_list = weeks_list.split(",")
                    new_weeks_list = []
                    for week in original_weeks_list:
                        week = re.sub(r"\'", "", week)
                        week = re.sub(r"\"", "", week)
                        week = re.sub(r"\\", "", week)
                        week = re.sub(r" ", "", week)
                        new_weeks_list.append(week)

                    new_weeks_list.append(date)
                    if debug == 1:
                        print("Updated new_weeks_list: ")
                        print(new_weeks_list)
                    getBook.weeks_list = new_weeks_list


                    getBook.save()

                else:
                    # Store the Amazon price of a given book
                    current_amazon_link = ''
                    amazon_price = 0.00
                    print("-- " + str(book['title']))
                    # use the following to debug amazon pricing feature
                    # book['amazon_product_url'] = 'http://www.amazon.com/Wildest-Dreams-Thunder-Point-Robyn/dp/0778317498'
                    if book['amazon_product_url'] is None:
                        # If no link is provided just use the amazon search, to get a link
                        print("No amazon url provided by NYT api")
                        # encode search title onto url appropriatly
                        amazon_search_title = book['title']
                        amazon_search_auth = book['contributor']

                        amazon_search_auth = urlencode({'': amazon_search_auth})
                        amazon_search_auth = re.sub('=', '+', amazon_search_auth)
                        amazon_search_title = urlencode({'': amazon_search_title})

                        amazon_search_url = "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords" \
                                            + amazon_search_title + amazon_search_auth

                        print("Created amazon search url " + str(amazon_search_url))

                        # get the search page
                        r = requests.get(amazon_search_url)
                        soup = BeautifulSoup(r.content, "lxml")
                        # find all links on the search page
                        links = soup.find_all("a")
                        g_data = soup.find_all("a", {"class": "a-link-normal a-text-normal"})
                        # Take the fisrt result's url
                        x = 0
                        index_of_Link = 0
                        # search for the link in the html
                        while x < len(g_data):
                            if re.match(".*\/dp\/.*", str(g_data[x])) is not None:
                                index_of_Link = x
                                # print(index_of_Link)
                                x = (len(g_data))
                            x = x + 1

                        current_amazon_price = float('0.00')
                        if (len(g_data)) > 0:
                            # var now contains the index of the link in the html list
                            var = str(g_data[index_of_Link])
                            # regex as required to get just the Book page URL
                            var = re.sub(".*href=\"", '', var)
                            current_amazon_link = re.sub("\".*", '', var)
                            print("Using first seach result link " + str(current_amazon_link))
                            myopener = MyOpener()
                            page = myopener.open(current_amazon_link)
                            html = page.read()
                            soup = BeautifulSoup(html, "lxml")

                            price = search(soup, "Paperback")
                            # regex to get rid of any excess text
                            price = cleanPrice(price)
                            # Handle an odd case where Amazon stores price as '-'...
                            if price == '—':
                                price = search(soup,'Hardcover')
                                price = cleanPrice(price)
                            current_amazon_price = float(price)
                        else:
                            print("Could not get a link from search results")
                            price = float('0.00')

                        print("Completed " + str(price))
                    else:
                        print("Using NYT provided Amazon link")
                        # Mask from amazon that we are using a script
                        current_amazon_link = book['amazon_product_url']
                        myopener = MyOpener()
                        page = myopener.open(current_amazon_link)
                        html = page.read()
                        soup = BeautifulSoup(html, "lxml")
                        new_html = soup.prettify()

                        price = search(soup, "Paperback")
                        # regex to get rid of any excess text
                        price = cleanPrice(price)
                        # Handle an odd case where Amazon stores price as '-'...
                        if price == '—':
                            price = search(soup,'Hardcover')
                            price = cleanPrice(price)
                        current_amazon_price = float(price)
                        print("Completed " + price)



                    isbn_list = []
                    BS_date_list = []
                    rank_list = []
                    isbn_list.append(book['primary_isbn13'])
                    isbns=book['isbns']
                    i = 0
                    for isbn13 in isbns: #loop to add one book per isbn13
                        isbn_list.append(isbns[i]['isbn13'])
                        i = i+1

                    NYT_image_url = book['book_image']
                    if NYT_image_url is None:
                        NYT_image_url = ""

                    # get rank and date information and store into respective lists 
                    rank = book['rank']
                    rank_list.append(rank)
                    BS_date_list.append(date)

                    b = Book(title=book['title'], author=contributor, isbns=isbn_list, NYT_image_url=NYT_image_url,
                             rating=0.00, goodreads_ID="", NYT_description=book['description'], weeks_list=BS_date_list,
                             weekly_rank=rank_list,amazon_price=current_amazon_price, amazon_link=current_amazon_link)

                    if debug == 1:
                        print("Created "+book['title'])
                    b.save()

                    dictionary = {'title': book['title'], 'author': contributor, 'goodreads_ID': 0,
                                      'isbn': isbn_list[0],'date_list':BS_date_list, 'NYT_ranks':rank_list}

                    # dictionary.append(BS_date_list)
                    if debug == 1:
                        print (dictionary)
                    temporary_books.append(dictionary)

            for book in temporary_books:
                foundAuthor = book['author']
                foundTitle = book['title']
                getBook = Book.objects.get(title=foundTitle, author=foundAuthor)
                if debug == 1:
                    print("for book "+foundTitle)
                if getBook.goodreads_ID == "":
                    
                   #1. Use API Search
                    print("starting API search for "+foundTitle)

                    #Work around required for initials
                        #as NYTimes does E L James while Goodreads does E.L. James 
                    result = re.search("([A-Z] [A-Z]) ", foundAuthor)
                    if result is not None:
                        result = result.group(1)
                        initials = re.sub(r" ", '', result)
                        foundAuthor = re.sub(r"[A-Z] [A-Z] ", initials[0]+initials[1]+" ", foundAuthor)
                        print("Author with two initials has been found and updated to: "+foundAuthor)
                    # periodAuthor = re.sub(, ,foundAuthor)
                    authorAndTitle = foundTitle+" "+foundAuthor
                    authorAndTitle.strip()
                    authorAndTitle = _strip_accents(authorAndTitle)
                    authorAndTitle = re.sub(r"[\s]", '%20', authorAndTitle)
                    if debug == 1:
                        print("authorAndTitle: "+authorAndTitle)
                    url = "https://www.goodreads.com/search/index.xml?key=Pq4UJeA708Z85U4IyHuAQ&q="+authorAndTitle
                 #   url = "https://www.goodreads.com/search/index.xml?key=Pq4UJeA708Z85U4IyHuAQ&search[author]&q=Four%20Week%20Fiance"
                    response = urllib.request.urlopen(url)
                    html = response.read()
                    text=html.decode()

                    ratings_count_list = re.findall("<ratings_count type=\"integer\">([^<]*)</ratings_count>", text)
                    Goodreads_ID_list = re.findall("<best_book type=\"Book\">[\s]*<id type=\"integer\">([^<]*)</id>", text)
                    average_rating_list = re.findall("<average_rating[^>]*>([^<]*)</average_rating", text)


                    title_list = re.findall("<title>([^<]*)</title>", text)

                    l = 0
                    lowest_difference_index = 0
                    lowest_difference = -1
                    for title in title_list:
                        title = title.upper()
                        title = re.sub('[\s]*\([^\)]*\)', '', title)
                        difference = distance(title, foundTitle)
                        if debug ==1:
                            print("for title: "+title)
                            print("difference: "+str(difference))
                        if lowest_difference == -1:
                            lowest_difference = difference
                    
                        if difference < lowest_difference:
                            if ratings_count_list[l] != "0":
                                lowest_difference = difference
                                lowest_difference_index = l
                                if debug == 1:
                                    print("updating lowest difference title to: "+ title)
                        l = l+1

                    GR_ID_value = Goodreads_ID_list[lowest_difference_index]
                    if debug == 1:
                        print("GR_ID: "+GR_ID_value)
                    ratingValue = average_rating_list[lowest_difference_index]

                   #2. Scrape genre and iamge
                    GR_ID_value.strip()
                    response = urllib.request.urlopen("http://www.goodreads.com/book/show/"+GR_ID_value)
                    html = response.read()
                    text = html.decode()

                    #print(text)
                    genre_list = re.findall("<div class=\"elementList \">[\s]*<div class=\"left\">[\s]*<a class=\"actionLinkLite\" href=\"[^\"]+\">([^<]+)", text)
                    if genre_list is not None:
                        if genre_list[0] == "Fiction":
                            genreValue = genre_list[1]
                            if debug == 1:
                                print("Top genre = Fiction. Have selected "+genreValue)
                        else:
                            genreValue = genre_list[0]
                    else:
                        genreValue = "ERROR"
     
                    GR_URL_value = re.search("<a href=\"[^\"]+\" itemprop=\"[^\"]+\" rel=\"[^\"]+\"><img alt=\"[^\"]+\" id=\"coverImage\" src=\"([^\"]+)", text)
                    if GR_URL_value is not None: 
                        GR_URL_value = GR_URL_value.group(1)
                    else:
                        GR_URL_value = "ERROR"                 
                    getBook.goodreads_ID = GR_ID_value
                    getBook.GR_image_url = GR_URL_value
                    getBook.rating = ratingValue
                    getBook.genre = genreValue
                    getBook.goodreads_URL = "http://www.goodreads.com/book/show/"+GR_ID_value
                    getBook.save()               
        
        print(datetime.datetime.time(datetime.datetime.now()))


    def handle(self, *args, **options):
        self._create_books()
