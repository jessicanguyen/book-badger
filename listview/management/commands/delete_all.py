from django.core.management.base import BaseCommand, CommandError
from listview.models import Book

class Command(BaseCommand):
    help = 'Deletes all book models'

    def _delete_books(self):
        Book.objects.all().delete()

    def handle(self, *args, **options):
        self._delete_books()