#Given a NYTimes Bestseller list (1 week's worth), creates model for all the books on the list

#To-do:
    #Don't make a model if one already exists for that book
    #Needs more fields i.e. past week's ranking
    #Doesn't display special chars i.e. "title":FOUR WEEK FIANC\u00c9","author":"JS Cooper"
    #New YOrk Times API image url - can't get it cos it has backslashes in it

#Notes from lecturer
    #It's fine to store all the data locally on a server
    #It's fine to have a script that you can run every week to update info 
        #as long as it doesn't require any extra user input i.e. editing the script

#Useful links
    #http://stackoverflow.com/questions/12344332/parsing-muilti-dimensional-json-array-to-python


#STRUCTURE
#For each book in the new york times list API
    #If one doesn't exist, create one and add:
        #title, author, week/rank, isbn13 array
    #If one does exist, add:
        #week/rank

#From all isbn13 arrays, create string
#Feed string into Goodreads API and get Goodreads IDs
#For each non-blank Goodreads ID
    #Find the corresponding book
    #If book currently has no Goodreads ID
        #Add Goodreads ID
        #Use scraper to addt rating, genre, image URL
#For each blank Goodreads ID
    #Find the corresponding book
    #If book currently has no Goodreads ID
        #Use Goodreads API search
        #Add rating, image URL, goodreads ID
        #Use scraper to add genre


from django.core.management.base import BaseCommand
import sys
import re
import json
import urllib.request

#response = urllib.request.urlopen("http://api.nytimes.com/svc/books/v3/lists/2015-09-01/combined-print-and-e-book-fiction?api-key=59a1ab5ad1cb73060391028ba36dfc86:8:72695504")

#html = response.read()
file = open('5booklist.txt', 'r')        
input = file.read()
parsed_input=json.loads(input)
results = parsed_input['results']
books = results['books']
isbn13s = [] #List with dicts inside with structure {Title, Author, ISBN, GR_ID}                
for book in books:
    print(book['title'])
    print(book['author'])
    isbns=book['isbns']
    dictionary={'title':book['title'],'author':book['author'], 'isbn':book['primary_isbn13'], 'goodreads_ID':0}
    isbn13s.append(dictionary)
    i = 0
    for isbn13 in isbns:
        dictionary={'title':book['title'],'author':book['author'], 'isbn':isbns[i]['isbn13'], 'goodreads_ID':0}
        isbn13s.append(dictionary)
        print(isbns[i]['isbn13'])
        i = i+1
    print(book['primary_isbn13'])

    print()
goodreads_isbn_string = ','.join(str(data['isbn']) for data in isbn13s)
goodreads_isbn_list = goodreads_isbn_string.split(",")
print(goodreads_isbn_string)
   
# url="https://www.goodreads.com/book/isbn_to_id/"+goodreads_isbn_string+"?key=Pq4UJeA708Z85U4IyHuAQ"
# response = urllib.request.urlopen(url)
# html = response.read()
# goodreads_ids = html.decode()
goodreads_ids = "25937666,,,,,,,,,"
print("goodreads_ids")
print(goodreads_ids)
goodreads_ID_list = goodreads_ids.split(",")



k = 0
for ID in goodreads_ID_list:
    if ID != "": #a goodreads ID exists for an isbn
        relevant_isbn = goodreads_isbn_list[k]
        for data in isbn13s: #find the matching title/author/isbn for the ID
            if data['isbn'] == relevant_isbn: 
                found = True
                break
        if found: #get info on the book
             ######## SCRAPER ################
                print(data['title']+"'s scraped goodreads data: ")
                ID.strip()
                response = urllib.request.urlopen("http://www.goodreads.com/book/show/"+ID)
                html = response.read()
                text = html.decode()

                genreValue = re.search("<div class=\"elementList \">[\s]*<div class=\"left\">[\s]*<a class=\"actionLinkLite\" href=\"[^\"]+\">([^<]+)", text)
                print(genreValue.group(1))

                ratingValue = re.search("ratingValue\">(\d.\d\d)", text) 
                print(ratingValue.group(1))

        #             #######################

        #                 # getBook.isbn=relevant_isbn
        #                 # getBook.goodreads_ID = ID
        #                 # getBook.rating = ratingValue
        #                 # getBook.genre = genreValue
        #                 # getBook.save()

            # k = k+1




#ISBN list from Goodreads
    #Four Week Fiance: 1516923251 
    #Love After Dark: 1942295359 (ISBN13: 9781942295358) 
    #Something Beautiful: 9781310478833, 9781512284041
    #Cocky Bastard:1515303942 (ISBN13: 9781515303947) 

#Find books by title, author or ISBN
#https://www.goodreads.com/search/index.xml?key=Pq4UJeA708Z85U4IyHuAQ&q=Four%20Week%20Fiance
    # <GoodreadsResponse><Request><authentication>true</authentication>
    # <key>Pq4UJeA708Z85U4IyHuAQ</key><method>search_index</method></Request><search>
    # <query>Four Week Fiance</query><results-start>1</results-start><results-end>3</results-end>
    # <total-results>3</total-results><source>Goodreads</source>
    # <query-time-seconds>0.06</query-time-seconds><results><work><id type="integer">45415205</id>
    # <books_count type="integer">4</books_count><ratings_count type="integer">1316</ratings_count>
    # <text_reviews_count type="integer">249</text_reviews_count>
    # <original_publication_year type="integer">2015</original_publication_year>
    # <original_publication_month type="integer">8</original_publication_month>
    # <original_publication_day type="integer">16</original_publication_day>
    # <average_rating>3.81</average_rating><best_book type="Book"><id type="integer">25605890</id>
    # <title>Four Week Fiancé (Four Week Fiancé, #0.5)</title><author>
    # <id type="integer">6604687</id><name>Helen    Cooper</name></author>
    # <image_url>https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png</image_url>