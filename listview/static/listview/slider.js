$(window).load(function(){
var dt_from = "2011-11-01";
var dt_to = "2015-11-24";
var dt_cur_from, dt_cur_to;


$('.slider-time').html(dt_from);
$('.slider-time2').html(dt_to);
var min_val = Date.parse(dt_from) / 1000;
var max_val = Date.parse(dt_to) / 1000;

function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
}

function formatDT(__dt) {
    var year = __dt.getFullYear();
    var month = zeroPad(__dt.getMonth() + 1, 2);
    var date = zeroPad(__dt.getDate(), 2);
    return date + '-' + month + '-' + year;
};


$("#slider-range").slider({
    range: true,
    min: min_val,
    max: max_val,
    step: 604800,
    values: [min_val, max_val],
    slide: function (e, ui) {
        dt_cur_from = new Date(ui.values[0] * 1000); //.format("yyyy-mm-dd hh:ii:ss");
        $('.slider-time').html(formatDT(dt_cur_from));

        dt_cur_to = new Date(ui.values[1] * 1000); //.format("yyyy-mm-dd hh:ii:ss");                
        $('.slider-time2').html(formatDT(dt_cur_to));

        if ($("#start_datepicker").length) {
            $("#start_datepicker").datepicker("setDate", dt_cur_from);
            $("#end_datepicker").datepicker("setDate", dt_cur_to);
        }
    }
});


$("#start_datepicker").datepicker({
    showOn: "button",
    buttonImage: "http://findicons.com/files/icons/1156/fugue/16/calendar_blue.png",
    buttonImageOnly: true,
    showAnim: "slideDown",
    showButtonPanel: true,
    autoSize: true,
    dateFormat: "d M, y",
    changeMonth: true,
    changeYear: true,
    minDate: new Date(dt_from),
    maxDate: new Date(dt_to),
    onSelect: function () {
        $('.slider-time').html(formatDT(new Date(this.value)));
        min_val = Date.parse(this.value) / 1000;
        $("#slider-range").slider("option", "values", [min_val, max_val]);
    }
});

$("#end_datepicker").datepicker({
    showOn: "button",
    buttonImage: "http://findicons.com/files/icons/1156/fugue/16/calendar_blue.png",
    buttonImageOnly: true,
    showAnim: "slideDown",
    showButtonPanel: true,
    autoSize: true,
    dateFormat: "d M, y",
    changeMonth: true,
    changeYear: true,
    minDate: new Date(dt_from),
    maxDate: new Date(dt_to),
    onSelect: function () {
        $('.slider-time2').html(formatDT(new Date(this.value)));
        max_val = Date.parse(this.value) / 1000;
        $("#slider-range").slider("option", "values", [min_val, max_val]);
    }
});
});//]]> 