import time
import datetime

from dateutil.parser import parse

original_dates_list = ['2015-01-04', '2015-01-11', '2015-01-18', '2015-01-25', '2015-02-01', '2015-02-08', '2015-02-15',
                       '2015-02-22', '2015-03-01', '2015-03-08', '2015-09-06', '2015-09-13', '2015-09-20', '2015-09-27']
original_ranks_list = ['1', '2', '3', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '9']
new_ranks = []
new_dates = []

if len(original_dates_list) < 2:
    # render the chart
    time.sleep(1)
else:
    # Put the starting date and its rank onto the new lists
    start_date = parse(original_dates_list[0])
    new_dates.append(start_date.strftime('%Y-%m-%d'))
    new_ranks.append(original_ranks_list[0])
    # Parse the end date for the loop
    end_date = parse(original_dates_list[len(original_dates_list) - 1])
    # Set up for the while loop
    curr_date = start_date
    while curr_date < end_date:
        # Add on 7 days from the starting day
        curr_date += datetime.timedelta(days=7)
        # Format it appropriately for compare
        date_to_check = curr_date.strftime('%Y-%m-%d')
        # Loop through the original_date_list using the index, so we can reference the appropriate element later,
        #  from each original list
        index = 1
        while index < len(original_dates_list):
            # If we get a match on a date_to_check with a date in the list we add it and the corresponding
            #  rank to the new lists
            if date_to_check == original_dates_list[index]:
                print(date_to_check + " IS in the list")
                new_dates.append(date_to_check)
                new_ranks.append(original_ranks_list[index])
            # elif we have reached the end of the list
            elif index == len(original_dates_list) - 1:
                # and cannot find date_to_check in the original dates list, append the date and an arbritairy rank to
                #  the new lists
                if date_to_check not in original_dates_list:
                    print(str(date_to_check) + " NOT in list")
                    new_ranks.append('21')
                    new_dates.append(date_to_check)

            index += 1
print("Completed...")
print(str(new_dates))
print(str(new_ranks))
print(len(new_ranks))
print(len(new_dates))
