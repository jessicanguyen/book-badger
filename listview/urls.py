from django.conf.urls import url
from . import views

#assigning a view called book_list to ^$ url
urlpatterns = [
	url(r'listview/', views.list_view, name='list_view'),
	url(r'^$', views.cover_mode, name='cover_mode'),
]