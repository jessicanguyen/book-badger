#Useful info:
    #http://stackoverflow.com/questions/1110153/what-is-the-most-efficent-way-to-store-a-list-in-the-django-models

from django.db import models
from django.utils import timezone
import re


class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    rating = models.CharField(max_length=5)
    genre = models.CharField(max_length=200)
    rank = models.CharField(max_length=5)
    NYT_image_url = models.CharField(max_length=200)
    GR_image_url = models.CharField(max_length=200)
    isbns = models.TextField(max_length=600)
    weekly_rank = models.TextField(max_length=1000)
    weeks_list = models.TextField(max_length=1000)
    goodreads_ID = models.CharField(max_length=25)
    goodreads_URL = models.CharField(max_length = 100)
    NYT_description = models.TextField(max_length = 500)
    amazon_link = models.CharField(max_length=400)
    amazon_price = models.DecimalField(max_digits=6, decimal_places=2)


    def publish(self):
       self.save()

    def __str__(self):
        return self.title
    
    def getTitle(self):
        return self.title

    def getAuthor(self):
        return self.author

    def getRating(self):
        return self.rating

    def getGenre(self):
        return self.genre

    def getNYT_Image_URL(self):
        return self.NYT_image_url

    def getGR_Image_URL(self):
        return self.GR_image_url

    def getISBNs(self):
        return self.isbns

    def getWeekly_Rank(self):
        return self.weekly_rank

    def getWeeks_List(self):
        return self.weeks_list

    def getGoodreads_ID(self):
        return self.goodreads_ID