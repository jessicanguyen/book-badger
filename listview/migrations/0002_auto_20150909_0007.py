# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listview', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='book',
            name='text',
        ),
        migrations.AddField(
            model_name='book',
            name='image_url',
            field=models.CharField(max_length=200, default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='isbn',
            field=models.CharField(max_length=200, default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='rating',
            field=models.CharField(max_length=5, default=0),
            preserve_default=False,
        ),
    ]
