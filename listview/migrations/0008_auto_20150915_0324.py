# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listview', '0007_auto_20150915_0306'),
    ]

    operations = [
        migrations.RenameField(
            model_name='book',
            old_name='image_url',
            new_name='NYT_image_url',
        ),
        migrations.AddField(
            model_name='book',
            name='GR_image_url',
            field=models.CharField(default=0, max_length=200),
            preserve_default=False,
        ),
    ]
