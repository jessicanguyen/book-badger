# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listview', '0013_auto_20150923_0532'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='rank',
            field=models.CharField(default=0, max_length=5),
            preserve_default=False,
        ),
    ]
