# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listview', '0002_auto_20150909_0007'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='rank',
            field=models.CharField(default=0, max_length=4),
            preserve_default=False,
        ),
    ]
