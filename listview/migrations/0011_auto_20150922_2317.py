# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listview', '0010_book_nyt_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='weeks_list',
            field=models.TextField(max_length=1000, default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='book',
            name='weekly_rank',
            field=models.TextField(max_length=1000),
        ),
    ]
