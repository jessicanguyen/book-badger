# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listview', '0014_book_rank'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='amazon_link',
            field=models.CharField(default='', max_length=400),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='amazon_price',
            field=models.DecimalField(default='0.00', max_digits=6, decimal_places=2),
            preserve_default=False,
        ),
    ]
