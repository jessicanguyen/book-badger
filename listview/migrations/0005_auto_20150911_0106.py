# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listview', '0004_auto_20150911_0059'),
    ]

    operations = [
        migrations.RenameField(
            model_name='book',
            old_name='isbns',
            new_name='isbn',
        ),
    ]
