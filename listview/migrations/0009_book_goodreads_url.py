# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listview', '0008_auto_20150915_0324'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='goodreads_URL',
            field=models.CharField(max_length=100, default=0),
            preserve_default=False,
        ),
    ]
