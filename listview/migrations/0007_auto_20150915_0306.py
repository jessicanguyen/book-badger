# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listview', '0006_auto_20150911_0143'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='book',
            name='isbn',
        ),
        migrations.RemoveField(
            model_name='book',
            name='rank',
        ),
        migrations.AddField(
            model_name='book',
            name='isbns',
            field=models.TextField(max_length=600, default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='book',
            name='weekly_rank',
            field=models.TextField(max_length=4, default=0),
            preserve_default=False,
        ),
    ]
