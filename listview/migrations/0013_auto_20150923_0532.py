# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listview', '0012_auto_20150923_0520'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='weeks_list',
            field=models.TextField(max_length=1000),
        ),
    ]
