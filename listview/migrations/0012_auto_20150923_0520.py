# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listview', '0011_auto_20150922_2317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='weeks_list',
            field=models.DateField(max_length=1000),
        ),
    ]
