# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listview', '0009_book_goodreads_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='NYT_description',
            field=models.TextField(default=0, max_length=500),
            preserve_default=False,
        ),
    ]
