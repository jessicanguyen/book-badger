# README #

### Issues Log ###
* " J.R. Ward" comes up first in alphabetical author order as there is a space at the beginning of the name


This is a very good tutorial if you're confused about Django:
    http://tutorial.djangogirls.org/en/django_orm/index.html
It helped me out a lot!

### First thing you do before anything else ###
Start up virtual environment:

    . myvenv/bin/activate

### Running the Server ###

    python manage.py migrate
    python manage.py runserver

Then go to this URL:
    http://127.0.0.1:8000/

### Running Commands ###

I have created some commands you can use from the root folder when deleting/showing the book objects:

    python manage.py delete_all
    python manage.py show_all


### Opening Shell ###

    python manage.py shell

### Creating/Deleting/Etc Models in Shell ###

You ALWAYS need to do this step each time you open shell:

    from listview.models import Book

To show all objects in the shell:

    Book.objects.all()

Creating new Book object in shell:

    Book.objects.create(title='', parameter='etc')

Deleting all Book objects:

    Book.objects.all().delete()

Filtering Book objects in shell:

    Book.objects.filter(genre='Fantasy')

    Book.objects.filter(title__contains='title')

Enacting an object's method:

    book = Book.objects.get(title="XYZ")

    book.publish()

Ordering objects:

    Book.objects.order_by('created_date')

    Book.objects.order_by('-created_date') # put a minus at the beginning and it reverses

Chaining QuerySets:

    Book.objects.filter(published_date__lte=timezone.now()).order_by('published_date')

Exiting the shell:

    exit()

### What to do after you've made changes to models.py file ###

If you make a change to the models.py file (add fields, etc) you need to do this before anything will work.


    python manage.py makemigrations listview

    python manage.py migrate listview

### Git ###

    git add *

    git commit -m "message"

    git push -all ?????


????? - this is whatever you named the link to the repo  