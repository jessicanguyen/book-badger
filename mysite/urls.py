from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)), #"for every URL that 
    	#starts with admin/, Django will find a corresponding view"

    url(r'', include('listview.urls')),
]
